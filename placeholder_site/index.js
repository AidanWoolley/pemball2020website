// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
    var playerWidth = window.innerHeight <= 420 ? window.innerWidth * 0.95 : window.innerWidth * 0.6;
    var playerHeight = playerWidth * 0.46875;
    player = new YT.Player('player', {
        height: playerHeight.toString(),
        width: playerWidth.toString(),
        videoId: '66w-I8SuXPI',
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
  });
}

function onPlayerReady(event) {
    event.target.mute();
    event.target.playVideo();
}

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.ENDED) {
        event.target.seekTo(0, true);
    }
}

window.onresize = function () {
    var player = document.getElementById("player");
    player.width = window.innerHeight <= 420 ? window.innerWidth * 0.95 : window.innerWidth * 0.6;
    player.height = player.width * 0.46875;
}

var isInViewport = function (elem) {
    var bounding = elem.getBoundingClientRect();
    return (
        bounding.top >= 0 &&
        bounding.left >= 0 &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
};


var questions = document.getElementsByClassName("question");
var i;

for (i = 0; i < questions.length - 1; i++) {
    questions[i].addEventListener("click", function() {
        this.classList.toggle("question_active");
        var answer = this.nextElementSibling;
        var nextQuestion = answer.nextElementSibling;
        if (answer.style.maxHeight) {
            nextQuestion.style.borderTop = "none";
            answer.style.maxHeight = null;
        } else {
            nextQuestion.style.borderTop = "1px solid var(--pontus)";
            answer.style.maxHeight = answer.scrollHeight + "px";

            setTimeout(function () {
                if (!isInViewport(answer)){
                    answer.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
                }
            }, 400);
        }
    });
}

// Special-case last item to aboid double-border issues
questions[questions.length - 1].addEventListener("click", function() {
    this.classList.toggle("question_active");
    var answer = this.nextElementSibling;
    if (answer.style.maxHeight) {
        answer.style.maxHeight = null;
    } else {
        answer.style.maxHeight = answer.scrollHeight + "px";

        setTimeout(function () {
            if (!isInViewport(answer)){
                answer.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
            }
        }, 400);
    }
});

function ticketLink() {
    alert("The ticketing website is not yet open.");
}