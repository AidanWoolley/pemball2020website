const observerConfig = {
    root: null,
    rootMargin: "0px 0px 100px 0px",
    threshold: 0
}

function loadImages(entries, self) {
    entries.forEach(element => {
        if (element.isIntersecting) {
            const target = element.target;
            const cardContentDiv = target.children[0].children[0];
            var headshot = document.createElement("IMG");
            headshot.src = `./res/headshots/${target.id}.jpg`;
            headshot.className = "headshot";
            cardContentDiv.insertBefore(headshot, cardContentDiv.childNodes[2]);
            self.unobserve(target);
        }
    });
}

const imgLoader = new IntersectionObserver(loadImages, observerConfig);

window.onload = function () {
    const cards = document.getElementsByClassName("card");
    for (var i = 0; i < cards.length; i++) {
        imgLoader.observe(cards[i]);
    }
};
