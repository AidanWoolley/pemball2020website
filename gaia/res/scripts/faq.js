var isInViewport = function (elem) {
    var bounding = elem.getBoundingClientRect();
    return (
        bounding.top >= 0 &&
        bounding.left >= 0 &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
};


var questions = document.getElementsByClassName("question");
var i;

for (i = 0; i < questions.length - 1; i++) {
    questions[i].addEventListener("click", function() {
        this.classList.toggle("question_active");
        var answer = this.nextElementSibling;
        var nextQuestion = answer.nextElementSibling;
        if (answer.style.maxHeight) {
            nextQuestion.style.borderTop = "none";
            answer.style.maxHeight = null;
            answer.classList.remove("active_answer_box");
        } else {
            answer.style.maxHeight = answer.scrollHeight + "px";
            answer.classList.add("active_answer_box");
            setTimeout(function () {
                if (!isInViewport(answer)){
                    answer.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
                }
            }, 400);
        }
    });
}

// Special-case last item to aboid double-border issues
questions[questions.length - 1].addEventListener("click", function() {
    this.classList.toggle("question_active");
    var answer = this.nextElementSibling;
    if (answer.style.maxHeight) {
        answer.style.maxHeight = null;
    } else {
        answer.style.maxHeight = answer.scrollHeight + "px";

        setTimeout(function () {
            if (!isInViewport(answer)){
                answer.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
            }
        }, 400);
    }
});
