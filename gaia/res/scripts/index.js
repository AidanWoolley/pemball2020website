var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
    var playerWidth = window.innerHeight <= 750 ? window.innerWidth * 0.95 : window.innerWidth * 0.6;
    var playerHeight = playerWidth * 0.46875;
    player = new YT.Player('player', {
        height: playerHeight.toString(),
        width: playerWidth.toString(),
        videoId: '66w-I8SuXPI',
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
  });
}

function onPlayerReady(event) {
    event.target.mute();
    event.target.playVideo();
}

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.ENDED) {
        event.target.seekTo(0, true);
    }
}

window.onresize = function () {
    var player = document.getElementById("player");
    player.width = window.innerHeight <= 750 ? window.innerWidth * 0.95 : window.innerWidth * 0.6;
    player.height = player.width * 0.46875;
}
