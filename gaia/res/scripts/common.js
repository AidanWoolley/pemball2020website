let body = document.getElementsByTagName("body")[0];
let header = document.getElementsByTagName("header")[0];
let main = document.getElementsByTagName("main")[0];
let header_margin = document.getElementById("header_margin");
let footer = document.getElementsByTagName("footer")[0];

function fixContentTopMargin() {
    header_margin.style.marginTop = `${header.offsetHeight}px`;
    body.style.backgroundPositionY = `${header.offsetHeight}px, ${header.offsetHeight}px`;
    // 93vh to account for 7vh of padding at top and bottom of footer
    main.style.minHeight = `calc(calc(93vh - ${header.offsetHeight}px) - ${footer.offsetHeight}px)`;
    if (screen.width > 750){
        navMenuVisible = true;
        navMenu.style.display = "flex";
    }
}

fixContentTopMargin();
window.addEventListener("resize", fixContentTopMargin);


let navMenuButton = document.getElementById("nav_menu_button");
let navMenu = document.getElementById("nav_bar");
let navMenuVisible = false;

navMenuButton.addEventListener("click", function () {
    if (navMenuVisible) {
        navMenu.style.display = "none";
    } else {
        navMenu.style.display = "flex";
    }
    navMenuVisible = !navMenuVisible;
    fixContentTopMargin();
});
